const Router = require('express');

const router = new Router();

router.get('/', (req, res, next) => {
    res.data = 'Hello World!';

    next();
});

module.exports = router;
