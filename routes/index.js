const healthRoutes = require('./health.routes');
const usersRoutes = require('./users.routes');
const statsRoutes = require('./stats.routes');
const transactionsRoutes = require('./transactions.routes');
const betsRoutes = require('./bets.routes');
const eventsRoutes = require('./events.routes');

module.exports = (app) => {
    app.use('/health', healthRoutes);
    app.use('/users', usersRoutes);
    app.use('/stats', statsRoutes);
    app.use('/transactions', transactionsRoutes);
    app.use('/bets', betsRoutes);
    app.use('/events', eventsRoutes);
};
