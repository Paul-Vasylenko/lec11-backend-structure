const Router = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const responseMiddleware = require('../middlewares/response.middleware');
const statsRepository = require('../repositories/stats.repository');
const router = new Router();

router.get(
    '/',
    authMiddleware,
    (req, res, next) => {
        res.data = statsRepository.getStats();
        next();
    },
    responseMiddleware,
);

module.exports = router;
