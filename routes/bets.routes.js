const Router = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const responseMiddleware = require('../middlewares/response.middleware');
const betModel = require('../models/bet');
const router = new Router();
const usersRepository = require('../repositories/users.repository');
const eventsRepository = require('../repositories/events.repository');
const oddsRepository = require('../repositories/odds.repository');
const betsRepository = require('../repositories/bets.repository');
const statsRepository = require('../repositories/stats.repository');
router.post(
    '/',
    authMiddleware,
    async (req, res, next) => {
        const newBet = {
            ...({ id, eventId, betAmount, prediction } = req.body),
        };
        const currentUser = req.user; //if user authorized
        const isValidResult = betModel.validate(newBet);
        if (isValidResult.error) {
            res.error.status = 400;
            res.error.message = 'Validation error';
            next();
        }
        if (+currentUser.balance < +newBet.betAmount) {
            res.error.status = 400;
            res.error.message = 'Not enough balance';
            next();
        }
        await usersRepository.updateById(currentUser.id, {
            balance: currentUser.balance - newBet.betAmount,
        });
        const currentEvent = eventsRepository.getById(newBet.eventId);
        if (!currentEvent) {
            res.error.status = 400;
            res.error.message = 'Event wasn`t found';
            next();
        }
        const currentOdd = oddsRepository.getById(currentEvent.oddsId);
        if (!currentOdd) {
            res.error.status = 400;
            res.error.message = 'Odd wasn`t found';
            next();
        }
        let multiplier;
        switch (newBet.prediction) {
            case 'w1':
                multiplier = odds.home_win;
                break;
            case 'w2':
                multiplier = odds.away_win;
                break;
            case 'x':
                multiplier = odds.draw;
                break;
        }
        let createdBet = betsRepository.add({
            ...newBet,
            multiplier,
            user_id: currentUser.id,
            event_id: currentEvent.id,
        });
        statsRepository.addBet();
        res.data = {
            ...createdBet,
            currentBalance,
        };
        next();
    },
    responseMiddleware,
);

module.exports = router;
