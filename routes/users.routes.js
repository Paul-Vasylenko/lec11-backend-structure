const Router = require('express');
const { userModel, updatedUserModel } = require('../models/user');
const usersRepository = require('../repositories/users.repository');
const router = new Router();
const responseMiddleware = require('../middlewares/response.middleware');
const authMiddleware = require('../middlewares/auth.middleware');
const statsRepository = require('../repositories/stats.repository');
const isIdValid = (id) => {
    var schema = joi
        .object({
            id: joi.string().uuid(),
        })
        .required();
    var isValidResult = schema.validate(id);
    return isValidResult ? true : false;
};

router.get(
    '/:id',
    async (req, res, next) => {
        try {
            const { id } = req.params;
            const isValidResult = isIdValid(id);
            if (!isValidResult) {
                res.error.status = 400;
                res.error.message = 'Validation error';
                next();
            }
            const foundUser = await usersRepository.getById(id);
            if (!foundUser) {
                res.error.status = 404;
                res.error.message = 'Not found error';
                next();
            }
            res.data = foundUser;
        } catch (err) {
            res.error = err;
        }
        next();
    },
    responseMiddleware,
);

router.post('/', async (req, res, next) => {
    const newUserBody = {
        ...req.body,
    };
    var isValidResult = userModel.validate(newUserBody);
    if (isValidResult.error) {
        res.error = {
            status: 400,
            message: isValidResult.error.details[0].message,
        };
        next();
        return;
    }
    newUserBody.balance = 0;
    const createdUser = await usersRepository.add(newUser);
    const accessToken = jwt.sign(
        {
            type: createdUser.type,
            id: createdUser.id,
        },
        process.env.JWT_SECRET,
    );
    res.data = {
        ...user,
        accessToken,
    };
    statsRepository.addUser();
    next();
});

router.put(
    '/:id',
    authMiddleware,
    async (req, res, next) => {
        const { id } = req.params;
        const isValidResult = isIdValid(id);
        if (!isValidResult) {
            res.error.status = 400;
            res.error.message = 'Validation error';
            next();
        }
        const currentUser = req.user;
        if (currentUser.id !== userId) {
            res.error = {
                status: 401,
                message: 'UserId mismatch',
            };
            next();
            return;
        }
        let updatedUser = {
            ...req.body,
        };
        if (updatedUser.balance) {
            res.error = {
                status: 400,
                message: '"balance" is not allowed',
            };
            next();
            return;
        }
        if (updatedUser.email) {
            const foundUser = await usersRepository
                .getBy({
                    email: updatedUser.email,
                })
                .filter((user) => user.id !== id);
            if (foundUser[0]) {
                res.error = {
                    status: 400,
                    message: `Key (email)=(${updatedUser.email}) already exists.`,
                };
                next();
            }
        }
        updatedUser = usersRepository.updateById(id, {
            ...updatedUser,
        });
        res.data = { ...updatedUser };
        next();
    },
    responseMiddleware,
);

module.exports = router;
