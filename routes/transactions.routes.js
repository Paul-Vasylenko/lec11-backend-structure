const Router = require('express');
const authAdminMiddleware = require('../middlewares/authAdmin.middleware');
const transactionModel = require('../models/transaction');
const transactionsRepository = require('../repositories/transactions.repository');
const usersRepository = require('../repositories/users.repository');
const router = new Router();
router.post('/', authAdminMiddleware, async (req, res, next) => {
    const transaction = {
        ...req.body,
    };
    const isValidResult = transactionModel.validate(transaction);
    if (isValidResult.error) {
        res.error.status = 400;
        res.error.message = transactionValidationError.details[0].message;
        next();
    }

    let user = await usersRepository.getById(newTransaction.userId);
    if (!user) {
        res.error.status = 400;
        res.error.message = 'user does not exist';
        next();
    }
    const addedTransaction = await transactionsRepository.add(newTransaction);
    const currentBalance = addedTransaction.amount + user.balance;
    //update new balance here
    user = await usersRepository.updateById(user.id, {
        balance: currentBalance,
    });
    res.data = {
        ...addedTransaction,
        currentBalance,
    };
    next();
});

module.exports = router;
