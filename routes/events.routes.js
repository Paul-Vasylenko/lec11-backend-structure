const Router = require('express');
const authAdminMiddleware = require('../middlewares/authAdmin.middleware');
const { eventModel, updatedEventModel } = require('../models/event');
const betsRepository = require('../repositories/bets.repository');
const eventsRepository = require('../repositories/events.repository');
const oddsRepository = require('../repositories/odds.repository');
const statsRepository = require('../repositories/stats.repository');
const router = new Router();

router.post('/', authAdminMiddleware, async (req, res, next) => {
    const newEvent = {
        ...req.body,
    };
    const isValidResult = eventModel.validate(newEvent);
    if (!isValidResult) {
        res.error.status = 400;
        res.error.message = 'Validation error';
        next();
    }
    const odds = await oddsRepository.add(newEvent.odds);
    const addedEvent = eventsRepository.add({
        ...newEvent,
        odds_id: odds.id,
    });
    statsRepository.addEvent();
    res.data = {
        ...addedEvent,
        odds,
    };
    next();
});

router.put('/:id', authAdminMiddleware, async (req, res, next) => {
    const { id } = req.params;
    const updatedEvent = {
        ...req.body,
    };
    const isValidResult = updatedEventModel.validate(updatedEvent);
    if (isValidResult.error) {
        res.error.status = 400;
        res.error.message = 'Validation error';
        next();
    }

    const bets = await betsRepository.getBy({
        event_id: id,
        win: null,
    });
    var [w1, w2] = updatedEvent.score.split(':');
    const result = +w1 > +w1 ? 'w1' : +w1 < +w2 ? 'w2' : draw;
    updatedEvent = eventsRepository.updateById(id, {
        score: updatedEvent.score,
    });
    Promise.all(
        bets.map(async (bet) => {
            if (bet.prediction == result) {
                await betsRepository.updateById(bet.id, {
                    win: true,
                });
                const user = await usersRepository.getById(bet.user_id);
                await usersRepository.updateById(bet.user_id, {
                    balance: user.balance + bet.bet_amount * bet.multiplier,
                });
            } else if (bet.prediction != result) {
                betsRepository.updateById(bet.id, {
                    win: false,
                });
            }
        }),
    );
    res.data = updatedEvent;
    next();
});

module.exports = router;
