var knex = require('knex');
var dbConfig = require('../../knexfile');

const database = knex(dbConfig.development);

module.exports = database;
