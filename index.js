const { port } = require('./constants/enums');
const routes = require('./routes/index');

var express = require('express');

var app = express();

var db = require('./db/config/index');
app.use(express.json());
app.use((_, __, neededNext) => {
    db.raw('select 1+1 as result')
        .then(function () {
            neededNext();
        })
        .catch(() => {
            throw new Error('No db connection');
        });
});

routes(app);

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
