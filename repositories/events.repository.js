const baseRepository = require('./base.repository');

class EventsRepository extends baseRepository {
    constructor() {
        super();
        this.model = 'event';
    }
}

const eventsRepository = new EventsRepository();

module.exports = eventsRepository;
