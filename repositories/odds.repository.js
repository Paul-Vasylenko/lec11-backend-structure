const baseRepository = require('./base.repository');

class OddsRepository extends baseRepository {
    constructor() {
        super();
        this.model = 'odd';
    }
}

const oddsRepository = new OddsRepository();

module.exports = oddsRepository;
