const initialStats = require('../constants/initialStats');

class StatsRepository {
    constructor() {
        this.stats = initialStats;
    }
    getStats() {
        return this.stats;
    }
    addUser() {
        this.stats.totalUsers++;
    }

    addBet() {
        this.stats.totalBets++;
    }

    addEvent() {
        this.stats.totalEvents++;
    }
}

const statsRepository = new StatsRepository();
module.exports = statsRepository;
