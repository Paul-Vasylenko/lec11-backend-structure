const baseRepository = require('./base.repository');

class TransactionsRepository extends baseRepository {
    constructor() {
        super();
        this.model = 'transaction';
    }
}

const transactionsRepository = new TransactionsRepository();

module.exports = transactionsRepository;
