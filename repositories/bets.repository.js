const baseRepository = require('./base.repository');

class BetsRepository extends baseRepository {
    constructor() {
        super();
        this.model = 'bet';
    }
}

const betsRepository = new BetsRepository();

module.exports = betsRepository;
