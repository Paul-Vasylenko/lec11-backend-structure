const baseRepository = require('./base.repository');

class UsersRepository extends baseRepository {
    constructor() {
        super();
        this.model = 'users';
    }
}

const usersRepository = new UsersRepository();

module.exports = usersRepository;
