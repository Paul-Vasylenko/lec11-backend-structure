const database = require('../db/config/index');

class baseRepository {
    constructor() {
        this.model = '';
    }

    getById(id) {
        return database(this.model)
            .where('id', id)
            .returning('*')
            .then(([model]) => model);
    }

    updateById(id, field, value) {
        return database(this.model)
            .where('id', id)
            .update(field, value)
            .returning('*')
            .then((model) => model);
    }
    getBy(condition) {
        return database(this.model)
            .where(condition)
            .returning('*')
            .then((models) => models)
            .catch((error) => {
                return null;
            });
    }
    add(model) {
        return database
            .insert(model)
            .returning('*')
            .then(([model]) => model);
    }
}

module.exports = baseRepository;
