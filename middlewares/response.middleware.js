const responseMiddleware = (req, res, next) => {
    if (res.error) {
        res.status(res.error.status ? res.error.status : 400).send(
            res.error.message ? res.error.message : 'Internal error',
        );
        return;
    }
    if (res.data) {
        res.status(200).send(res.data);
        return;
    }
    next();
};

module.exports = responseMiddleware;
