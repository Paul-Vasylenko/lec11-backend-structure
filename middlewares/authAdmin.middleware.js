const authAdminMiddleware = async (req, res, next) => {
    let token = req.headers['authorization'];
    let tokenPayload;
    if (!token) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
        tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        if (!tokenPayload) {
            throw new ResponseError('Not Authorized', 401);
        }
        if (tokenPayload.type !== 'admin') {
            throw new ResponseError('Not Authorized', 401);
        }
        const user = await usersRepository.getById(tokenPayload.id);
        req.user = user;
        next();
    } catch (err) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
};

module.exports = authAdminMiddleware;
