const jwt = require('jsonwebtoken');
const usersRepository = require('../repositories/users.repository');

const authMiddleware = async (req, res, next) => {
    let token = req.headers['authorization'];
    let tokenPayload;
    if (!token) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
        tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        const user = await usersRepository.getById(tokenPayload.id);
        req.user = user;
        next();
    } catch (err) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
};

module.exports = authMiddleware;
